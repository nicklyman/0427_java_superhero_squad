import org.junit.*;
import static org.junit.Assert.*;

public class SquadTest {

  @Test
  public void getSquadMember_getsNameOfHeroInSquad_String() {
    Superhero testSuperhero = new Superhero("Spider-man", 18, "Spidey Senses", "Emotional Issues");
    Squad testSquad = new Squad("Epic Feats", "Save the world");
    testSquad.addHero(testSuperhero);
    assertEquals("Spider-man", testSquad.getSquadMember(0));
  }

  @Test
  public void getSquadMember_fillsSquadUpToMaxSize_String() {
    Superhero testSuperhero = new Superhero("Spider-man", 18, "Spidey Senses", "Emotional Issues");
    Superhero testSuperhero2 = new Superhero("Not Spider-man", 56, "Powers? Ha!", "Everything");
    Squad testSquad = new Squad("Spidey Guys", "Be Pider-man :3");
    testSquad.addHero(testSuperhero);
    testSquad.addHero(testSuperhero);
    testSquad.addHero(testSuperhero);
    testSquad.addHero(testSuperhero);
    testSquad.addHero(testSuperhero2);
    String listOfHeroes = testSquad.getSquadMember(0) + " " + testSquad.getSquadMember(1) + " " + testSquad.getSquadMember(2) + " " + testSquad.getSquadMember(3);
    assertEquals("Spider-man Spider-man Spider-man Spider-man", listOfHeroes);
  }
}
