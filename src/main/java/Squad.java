import java.util.ArrayList;

public class Squad {
  private String mSquadName;
  private String mSquadCause;
  private static ArrayList<Superhero> mSquadHeroes = new ArrayList<Superhero>();
  public static final int MAX_SIZE = 4;

  public Squad(String squadName, String squadCause) {
    mSquadName = squadName;
    mSquadCause = squadCause;
  }

  public String getSquadMember(int heroId) {
    String name = mSquadHeroes.get(heroId).getName();
    return name;
  }

  public ArrayList<Superhero> getHeroes() {
    return mSquadHeroes;
  }

  public String getSquadName() {
    return mSquadName;
  }

  public String getSquadCause() {
    return mSquadCause;
  }

  public void addHero(Superhero hero) {
    if (mSquadHeroes.size() < MAX_SIZE) {
      mSquadHeroes.add(hero);
    }
  }

}
