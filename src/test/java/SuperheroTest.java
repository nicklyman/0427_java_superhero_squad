import org.junit.*;
import static org.junit.Assert.*;

public class SuperheroTest {

  @Test
  public void superhero_instantiatesCorrectly() {
    Superhero mySuperhero = new Superhero("Spider-man", 18, "Spidey Senses", "Emotional Issues");
    assertEquals("Spider-man", mySuperhero.getName());
    assertEquals(18, mySuperhero.getAge());
    assertEquals("Spidey Senses", mySuperhero.getPower());
    assertEquals("Emotional Issues", mySuperhero.getWeakness());
  }
}
